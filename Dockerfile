FROM ubuntu:18.04

LABEL version="1.0" maintainer="SUAU Adrien <adrien.suau@cerfacs.fr"

ARG ROOT_DIR="/"
ARG USER_DIR="/user"
ARG QATHS_DIR="${USER_DIR}/qaths"
ARG EXTERNAL_LIBS_DIR="${QATHS_DIR}/third_party"
ARG MYQLM_DIR="${EXTERNAL_LIBS_DIR}/myqlm/0.0.6/"
ARG MYQLM_PACKAGES_DIR="${MYQLM_DIR}/myqlm-0.0.6"

# Copy the whole project to the container
COPY ./ ${QATHS_DIR}
# Untar everything needed
RUN tar xvf ${EXTERNAL_LIBS_DIR}/myqlm-0.0.6.tar -C ${EXTERNAL_LIBS_DIR}
RUN tar xvzf ${MYQLM_DIR}/myqlm-0.0.6.tgz -C ${MYQLM_DIR}

# See https://askubuntu.com/a/1013396
ENV DEBIAN_FRONTEND=noninteractive

# Update some development libraries for numpy
RUN apt-get update
RUN apt-get install -y python3 python3-pip python3-numpy python3-matplotlib python3-scipy
RUN python3 -m pip install -U pip

# Install MyQLM
RUN python3 -m pip install ${MYQLM_PACKAGES_DIR}/qat_* ${MYQLM_PACKAGES_DIR}/myqlm_*

# Install qaths
RUN python3 -m pip install -e ${QATHS_DIR}
