# `qaths`

# Installation

Several options are available to install `qaths` on your local machine:

1. Using `pip`.
2. Using `docker`.

Both options are comparable is terms of performance. The `pip` version is preferred for
users that are experienced with python virtual environments (`conda` may be required if
your Python version does not match the required `3.6` version) and for user that will
use regularly or develop in the library. The `docker` option is the preferred 
alternative for users that just want to run the provided command line interfaces without
developing in the library. 

## Cloning the `git` repository

`qaths` is currently only accessible via `git`. Before considering installing it, you 
should retrieve the code from the `repository` with
```shell script
git clone git@gitlab.com:cerfacs/qaths.git
```

## Install with `pip`

In order to install `qaths` with pip you should check that:

1. Your Python version is `3.6` with `python --version`. If your Python version is `3.5`
   or lower or `3.7` or higher, you will need to have access to a Python `3.6` 
   executable. This can be done with [`conda`](https://docs.conda.io/en/latest/) by 
   using `conda create -n env python=3.6`.
2. Once you have a Python `3.6`, you should install the MyQLM external library stored in 
   the [`third_party`](third_party/) folder. This can be done with
   ```shell script
   cd qaths/external_libraries
   tar xvf myqlm-0.0.6-total.tar && cd myqlm/0.0.6/total
   # /!\ Do not forget to read the EULA of MyQLM. /!\
   unzip myQLM_EULA.zip
   tar xvzf myqlm-0.0.6-total.tgz && cd myqlm-0.0.6-total
   # Check that your Python version is 3.6.X
   python --version
   python -m pip install qat_* myqlm_*
   cd ../../../../../  # Go back to qaths/ directory
   # Test that the installation worked
   python -c "import qat; print('MyQLM successfully installed!')"
   ```
3. Once MyQLM is installed, install `qaths` with a regular `pip` command:
   ```shell script
   python -m pip install ./
   ```
   You can add a `-e` flag to `pip` in order to install `qaths` in "editable" or 
   "developer" mode. 

## Install with `docker`

To use `qaths` with the provided `docker` image run:

```shell script
docker login registry.gitlab.com
docker pull registry.gitlab.com/cerfacs/qaths
docker run --rm -it registry.gitlab.com/cerfacs/qaths bash
```


# Replicating the results

All the plots provided in [Practical Quantum Computing: solving the wave equation using
 a quantum approach](https://arxiv.org/abs/2003.12458) can be replicated and the raw 
data used to plot is 100% replicable.

In order to check/replicate the results, install `qaths` following the instructions in 
the [Installation](#installation) section. [Using Docker](#install-with-docker) is the
recommended way.

Once installed, several commands should have been exported into your environment.
Type `qaths.` and try to auto-complete with `TAB` to see the available commands.
All the commands are also listed in the [setup.py](setup.py) file.

If you have issue replicating results, please open an issue on Gitlab describing the 
issue.
