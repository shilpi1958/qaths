# ======================================================================
# Copyright CERFACS (February 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

"""Module implementing Hamiltonian simulation for multiples of the identity matrix.

This module contains the functions used to simulate a 1-sparse Hamiltonian that is a
multiple of the identity matrix.

The circuit returned by these procedures simulate exactly the given Hamiltonian.
"""

from qaths.simulation.base._routines import rzz
from qat.lang.AQASM.routines import QRoutine
from qat.lang.AQASM.misc import build_gate


@build_gate("simulate_multiple_of_identity", [float, int, float], lambda a, n, t: n)
def simulate_multiple_of_identity(alpha: float, n: int, time: float) -> QRoutine:
    r"""Simulate the evolution under a multiple of the identity as Hamiltonian.

    The returned routine needs :math:`n` qubits organised as follow: ::

        |        |x>        |
        |   .   .   .   .   |
        |  0           n-1  |

    * x is the starting state of the evolution
      :math:`\left(\vert x \rangle = \vert \phi_0 \rangle\right)`.

    :param alpha: The coefficient in front of the identity matrix. The simulated
        Hamiltonian will be :math:`\alpha \mathbf{I}`.
    :param n: The number of qubits the simulated Hamiltonian acts on.
    :param time: The duration of the desired evolution.
    """
    routine = QRoutine()
    qubits = routine.new_wires(n)
    routine.apply(rzz(-alpha * time), qubits[-1])

    return routine
