# ======================================================================
# Copyright CERFACS (January 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

"""Module implementing Hamiltonian simulation for 1-sparse permutation Hamiltonians.

This module contains the functions used to simulate a 1-sparse Hamiltonian that contain
either a "1" in each line/column or a "+/- i" in each line/column.

The circuit returned by these procedures simulate exactly the given Hamiltonian.
"""

from qat.lang.AQASM.gates import RZ

from qaths.simulation.base._routines import A, G, Two_qubits_parity_gate
from qat.lang.AQASM.routines import QRoutine
from qat.lang.AQASM.misc import build_gate
from qat.lang.AQASM.gates import AbstractGate


@build_gate(
    "simulate_real_permutation_hamiltonian",
    [str, int, float],
    arity=lambda name, n, t: n,
)
def simulate_real_permutation_hamiltonian(
    oracle_name: str, n: int, time: float
) -> QRoutine:
    r"""Simulate the evolution under an Hamiltonian with one 1 in each row/column.

    Let :math:`n` be the size of :math:`\ket{x}`.
    The returned routine needs :math:`n` qubits organised as follow: ::

        |        |x>        |
        |   .   .   .   .   |
        |   0          n-1  |

    * x is the starting state of the evolution
      :math:`\left(\vert x \rangle = \vert \phi_0 \rangle\right)`.

    :param oracle_name: name of the oracle that encodes the Hamiltonian matrix we
        want to simulate.
        The name will be used to create an AbstractGate, that can be linked afterwards.

        The oracle :math:`O` take as input :math:`\ket{x}\ket{0}`
        organised as follow: ::

            |        |x>        |        |m>        |
            |   .   .   .   .   |   .   .   .   .   |
            |  0           n-1  |   n         2*n-1 |

        and outputs the quantum state
        :math:`\ket{x}\ket{m(x)}` with:

        #. :math:`\ket{x}` encoding the index of the considered row (all the indices are
           in superposition).
        #. :math:`\ket{m(x)}` encoding the index of the column of the only non-zero
           element in the column of index :math:`x`.

    :param n: The number of qubits the simulated Hamiltonian acts on.
    :param time: The duration of the desired evolution.
    """
    routine = QRoutine()
    x = routine.new_wires(n)
    m = routine.new_wires(n)
    p = routine.new_wires(1)
    routine.set_ancillae(m)
    routine.set_ancillae(p)

    oracle = AbstractGate(oracle_name, [], arity=2 * n)

    # First, apply the oracle on the provided qubits
    routine.protected_apply(oracle(), x, m)
    routine.protected_apply(A(n), x, m, p)
    routine.apply(RZ(2 * time), p)
    routine.protected_apply(A(n).dag(), x, m, p)
    routine.protected_apply(oracle().dag(), x, m)

    return routine


@build_gate(
    "simulate_imaginary_permutation_hamiltonian",
    [str, int, float],
    arity=lambda name, n, t: n,
)
def simulate_imaginary_permutation_hamiltonian(
    oracle_name: str, n: int, time: float
) -> QRoutine:
    r"""Simulate the evolution under an Hamiltonian with one :math:`i` or :math:`-i` \
    in each row/column.

    Let :math:`n` be the size of :math:`\vert x \rangle`.
    The returned routine needs :math:`n` qubits organised as follow: ::

        |        |x>        |
        |   .   .   .   .   |
        |   0          n-1  |

    * x is the starting state of the evolution
      :math:`\left(\vert x \rangle = \vert \phi_0 \rangle\right)`.

    :param oracle_name:name of the oracle that encodes the Hamiltonian matrix we
        want to simulate.
        The name will be used to create an AbstractGate, that can be linked afterwards.

        The oracle :math:`O` take as input :math:`\ket{x}\ket{0}\ket{0}`
        organised as follow: ::

            |        |x>        |        |m>        |  |s>  |
            |   .   .   .   .   |   .   .   .   .   |   .   |
            |  0           n-1  |   n         2*n-1 |   2n  |

        and outputs the quantum state
        :math:`\ket{x}\ket{m(x)}\ket{s(x)} with:

        #. :math:`\ket{x}` encoding the index of the considered row (all the indices are
           in superposition).
        #. :math:`\ket{m(x)}` encoding the index of the column of the only non-zero
           element in the column of index :math:`x`.
        #. :math:`\ket{s(x)}` encoding the sign of the only non-zero entry in the column
           of index :math:`x` with the convention

           .. math::

              s(x) = \left\{\begin{array}{lr}
              0 & \text{if } x \geqslant 0 \\
              1 & \text{else} \\
              \end{array}\right.

    :param n: The number of qubits the simulated Hamiltonian acts on.
    :param time: The duration of the desired evolution.
    """
    routine = QRoutine()
    x = routine.new_wires(n)
    m = routine.new_wires(n)
    p = routine.new_wires(1)
    s = routine.new_wires(1)
    p2 = routine.new_wires(1)
    routine.set_ancillae(p)
    routine.set_ancillae(s)
    routine.set_ancillae(p2)

    oracle = AbstractGate(oracle_name, [], arity=2 * n + 1)

    # First, apply the oracle on the provided qubits
    routine.protected_apply(oracle(), x, m, s)
    routine.protected_apply(A(n), x, m, p)
    routine.protected_apply(G(), s)
    routine.protected_apply(Two_qubits_parity_gate(), p, s, p2)

    # TODO: explain why we need a minus here.
    # Linked with the problem of negative sign representation (in the paper it's
    # positive -> |0>, negative -> |1>, in practice only the opposite works).
    routine.apply(RZ(-2 * time), p2)

    routine.protected_apply(Two_qubits_parity_gate().dag(), p, s, p2)
    routine.protected_apply(G().dag(), s)
    routine.protected_apply(A(n).dag(), x, m, p)
    routine.protected_apply(oracle().dag(), x, m, s)

    return routine
