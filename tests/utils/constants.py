# ======================================================================
# Copyright CERFACS (April 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy

FLOAT_DISCRETISATION = numpy.array(
    [
        -1.6,  # Negative number
        -1e-5,  # Small negative number
        0,  # Zero
        1e-5,  # Small positive number
        numpy.pi,  # Pi (positive number)
    ]
)

FLOAT_DISCRETISATION_NO_ZERO = FLOAT_DISCRETISATION[
    numpy.abs(FLOAT_DISCRETISATION) > 1e-30
]

FLOAT_DISCRETISATION_POSITIVE = FLOAT_DISCRETISATION[FLOAT_DISCRETISATION > 0]

EPSILONS = numpy.power(10.0, -numpy.arange(2, 7))

RANDOMISED_REPEAT = 10

SIMULABLE_QUBITS = 10
DENSE_MATRIX_MAX_QUBITS = 10
PARAMETER_DISCRETISATION = 10
