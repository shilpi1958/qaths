# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy
import pytest

from qaths.utils import constants
from tests.utils.decorators import needsqat, randomised
from qaths.utils.simulation import routine2unitary
from qaths.utils.math import expm, kron, generate_random_hermitian_permutation
from tests.utils import matrices
from qaths.qram import PermutationSignQRAM, QRAM
import tests.utils.constants as test_constants

import qaths.simulation.base.permutation as permutation_routines


def _test_matrix_on_simulate_permutation_hamiltonian(
    oracle, matrix_to_simulate: numpy.ndarray, time: float, n: int
):
    routine = permutation_routines.simulate_real_permutation_hamiltonian(
        oracle, n, time
    )
    expected_routine_arity = 2 * n + 1
    routine_ancilla_size = routine.arity - expected_routine_arity

    expected_matrix = kron(
        expm(-1.0j * time * matrix_to_simulate),
        numpy.identity(2 ** (expected_routine_arity - n)),
        numpy.identity(2 ** routine_ancilla_size) if routine_ancilla_size > 0 else 1,
    )
    simulated_matrix = routine2unitary(routine)

    # We restrict ourselves to the cases where |m> = |a> = |0> because this is a
    # precondition of the simulate_real_permutation_hamiltonian function.
    # This means that every row of the expected or simulated matrix that correspond
    # to a quantum state with either |m> or |a> not equal to |0> must be ignored.
    rows_to_test = numpy.arange(0, 2 ** routine.arity, 2 ** (routine.arity - n))

    numpy.testing.assert_allclose(
        expected_matrix[rows_to_test],
        simulated_matrix[rows_to_test],
        atol=constants.ABSOLUTE_TOLERANCE,
    )


def _test_matrix_on_simulate_imaginary_permutation_hamiltonian(
    oracle, matrix_to_simulate: numpy.ndarray, time: float, n: int
):
    routine = permutation_routines.simulate_imaginary_permutation_hamiltonian(
        oracle, n, time
    )
    expected_routine_arity = 2 * n + 3
    routine_ancilla_size = routine.arity - expected_routine_arity

    expected_matrix = kron(
        expm(-1.0j * time * matrix_to_simulate),
        numpy.identity(2 ** (expected_routine_arity - n)),
        numpy.identity(2 ** routine_ancilla_size) if routine_ancilla_size > 0 else 1,
    )

    simulated_matrix = routine2unitary(routine)

    # We restrict ourselves to the cases where |m> = |p> = |s> = |p2> = |a> = |0>
    # because this is a precondition of the simulate_real_permutation_hamiltonian function.
    # This means that every row of the expected or simulated matrix that correspond
    # to a quantum state with either |m>, |p>, |p2>, |s> or |a> not equal to |0> must be
    # ignored.
    rows_to_test = numpy.arange(0, 2 ** routine.arity, 2 ** (routine.arity - n))

    numpy.testing.assert_allclose(
        expected_matrix[rows_to_test],
        simulated_matrix[rows_to_test],
        atol=constants.ABSOLUTE_TOLERANCE,
    )


@needsqat
@pytest.mark.parametrize(
    "n, matrix_to_simulate",
    # Simulation of the identity matrix for different sizes
    [(i, numpy.identity(2 ** i)) for i in range(1, 4)]
    # Simulation of the X matrix
    + [(1, matrices.X)]
    # Simulation of the CX matrix
    + [(2, matrices.CX)],
)
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
def test_simulate_permutation_hamiltonian(
    n: int, matrix_to_simulate: numpy.ndarray, time: float
):
    matrix_to_simulate = numpy.identity(2 ** n)
    oracle = QRAM(matrix_to_simulate, msb_first=True)
    _test_matrix_on_simulate_permutation_hamiltonian(
        oracle, matrix_to_simulate, time, n
    )


@needsqat
@pytest.mark.parametrize("n", range(1, 4))
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@randomised(5)
def test_simulate_permutation_hamiltonian_randomised(n: int, time: float):
    hermitian_permutation = generate_random_hermitian_permutation(n)
    matrix_to_simulate = numpy.identity(2 ** n)[hermitian_permutation]
    oracle = QRAM(matrix_to_simulate, msb_first=True)
    _test_matrix_on_simulate_permutation_hamiltonian(
        oracle, matrix_to_simulate, time, n
    )


@needsqat
@pytest.mark.parametrize(
    "n, matrix_to_simulate",
    # Simulation of the Y matrix
    [(1, matrices.Y)],
)
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
def test_simulate_imaginary_permutation_hamiltonian(
    n: int, matrix_to_simulate: numpy.ndarray, time: float
):
    oracle = PermutationSignQRAM(matrix_to_simulate.imag)
    _test_matrix_on_simulate_imaginary_permutation_hamiltonian(
        oracle, matrix_to_simulate, time, n
    )


@needsqat
@pytest.mark.parametrize("n", range(1, 3))
@pytest.mark.parametrize("time", test_constants.FLOAT_DISCRETISATION)
@randomised(5)
def test_simulate_imaginary_permutation_hamiltonian_randomised(n: int, time: float):
    hermitian_permutation, conj = generate_random_hermitian_permutation(
        n, include_conj=True
    )

    matrix_to_simulate = numpy.identity(2 ** n)[hermitian_permutation] * 1.0j
    for row in range(2 ** n):
        if conj[row]:
            matrix_to_simulate[row] = numpy.conj(matrix_to_simulate[row])

    oracle = PermutationSignQRAM(matrix_to_simulate.imag)
    _test_matrix_on_simulate_imaginary_permutation_hamiltonian(
        oracle, matrix_to_simulate, time, n
    )
