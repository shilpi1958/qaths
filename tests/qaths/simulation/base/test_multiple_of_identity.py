# ======================================================================
# Copyright CERFACS (April 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import pytest
import numpy

from qaths.simulation.base.multiple_of_identity import simulate_multiple_of_identity
from tests.utils.decorators import needsqat
from qaths.utils.simulation import routine2unitary
from qaths.utils.math import kron
from tests.utils.matrices import I
import tests.utils.constants as constants


@needsqat
@pytest.mark.parametrize("n", range(1, constants.SIMULABLE_QUBITS))
@pytest.mark.parametrize("alpha", constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
def test_simulate_multiple_of_identity(n: int, alpha: float, time: float):
    routine = simulate_multiple_of_identity(alpha, n, time)
    ancilla_size = routine.arity - n
    expected_matrix = kron(
        numpy.diag(numpy.exp(-1.0j * time * alpha) * numpy.ones((2 ** n,))),
        *[I for _ in range(ancilla_size)]
    )
    implemented_matrix = routine2unitary(routine)
    numpy.testing.assert_allclose(expected_matrix, implemented_matrix, atol=1e-7)
