# ======================================================================
# Copyright CERFACS (March 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy
import pytest

from tests.utils.decorators import needsqat, usessimulator
from qaths.utils.quantum import qstate_msb
from qaths.utils.simulation import simulate_routine, routine2unitary
from qaths.utils.math import expm, kron
from tests.utils.matrices import F, Z, I
from qaths.utils.endian import BIG_ENDIAN
import tests.utils.constants as constants
from qaths.bugfix.small_rotations_angle import bugfix_small_angles
from qaths.utils.constants import ABSOLUTE_TOLERANCE

import qaths.simulation.base._routines as routines


@needsqat
def test_W_routine():
    expected_matrix = numpy.array(
        [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1 / numpy.sqrt(2), 1 / numpy.sqrt(2), 0.0],
            [0.0, 1 / numpy.sqrt(2), -1 / numpy.sqrt(2), 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ],
        dtype=numpy.complex,
    )
    implemented_matrix = routine2unitary(routines.W)
    numpy.testing.assert_allclose(
        expected_matrix, implemented_matrix, atol=ABSOLUTE_TOLERANCE
    )


@needsqat
@pytest.mark.parametrize(
    "theta", numpy.linspace(0, 2 * numpy.pi, constants.PARAMETER_DISCRETISATION)
)
def test_Rzz_routine(theta: float):
    expected_matrix = numpy.array(
        [[numpy.exp(1.0j * theta), 0.0], [0.0, numpy.exp(1.0j * theta)]]
    )
    implemented_matrix = routine2unitary(routines.rzz(theta))
    numpy.testing.assert_allclose(
        expected_matrix, implemented_matrix, atol=ABSOLUTE_TOLERANCE
    )


@needsqat
@pytest.mark.parametrize(
    "theta", numpy.linspace(0, 2 * numpy.pi, constants.PARAMETER_DISCRETISATION)
)
def test_Rzz_routine_period(theta: float):
    expected_matrix = numpy.array(
        [[numpy.exp(1.0j * theta), 0.0], [0.0, numpy.exp(1.0j * theta)]]
    )
    implemented_matrix_2pi = routine2unitary(routines.rzz(theta + 2 * numpy.pi))
    numpy.testing.assert_allclose(
        expected_matrix, implemented_matrix_2pi, atol=ABSOLUTE_TOLERANCE
    )


@needsqat
def test_G_routine():
    expected_matrix = (-1 / numpy.sqrt(2)) * numpy.array([[1.0j, 1.0], [1.0, 1.0j]])
    implemented_matrix = routine2unitary(routines.G)
    numpy.testing.assert_allclose(expected_matrix, implemented_matrix)


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "given_input, expected_output",
    [
        # Here only input states with the last qubit to |0> are of interest
        # because ancillas are required to start in the state |0>.
        (qstate_msb("000"), qstate_msb("000")),
        (qstate_msb("010"), qstate_msb("010")),
        (qstate_msb("100"), qstate_msb("101")),
        (qstate_msb("110"), qstate_msb("110")),
    ],
)
def test_toffoli_10_routine(given_input: numpy.ndarray, expected_output: numpy.ndarray):
    simulated_output = simulate_routine(
        routines.Toffoli_10,
        given_input,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    numpy.testing.assert_allclose(
        expected_output, simulated_output, atol=ABSOLUTE_TOLERANCE
    )


@needsqat
def test_A_routine():
    pass


@needsqat
@usessimulator
@pytest.mark.parametrize(
    "given_input, expected_output",
    [
        # Here only input states with the last qubit to |0> are of interest
        # because ancillas are required to start in the state |0>.
        (qstate_msb("000"), qstate_msb("000")),
        (qstate_msb("010"), qstate_msb("011")),
        (qstate_msb("100"), qstate_msb("101")),
        (qstate_msb("110"), qstate_msb("110")),
    ],
)
def test_Two_qubits_parity_gate_routine(
    given_input: numpy.ndarray, expected_output: numpy.ndarray
):
    simulated_output = simulate_routine(
        routines.Two_qubits_parity_gate,
        given_input,
        starting_state_endianness=BIG_ENDIAN,
        output_state_endianness=BIG_ENDIAN,
    )
    numpy.testing.assert_allclose(
        expected_output, simulated_output, atol=ABSOLUTE_TOLERANCE
    )


@needsqat
@pytest.mark.parametrize("n", range(1, constants.SIMULABLE_QUBITS - 1))
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
def test_exp_ZFt_matrix(n: int, time: float):
    routine = routines.exp_ZFt(n, time)
    ancilla_size = routine.arity - n - 1
    expiZFt = expm(-1.0j * time * kron(Z, F(n), *[I for _ in range(ancilla_size)]))
    implemented_matrix = routine2unitary(routine)
    numpy.testing.assert_allclose(expiZFt, implemented_matrix, atol=ABSOLUTE_TOLERANCE)


@needsqat
@pytest.mark.parametrize("r", range(1, (constants.SIMULABLE_QUBITS - 1) // 3))
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
def test_exp_ZF2rt_matrix(r: int, time: float):
    routine = routines.exp_ZF2r_t(r, time)
    ancilla_size = routine.arity - 3 * r - 1
    expiZF2rt = expm(
        -1.0j
        * time
        * 2 ** (-2 * r)
        * kron(Z, F(3 * r), *[I for _ in range(ancilla_size)])
    )
    implemented_matrix = routine2unitary(routine)
    numpy.testing.assert_allclose(
        expiZF2rt, implemented_matrix, atol=bugfix_small_angles(ABSOLUTE_TOLERANCE)
    )


@needsqat
@pytest.mark.parametrize("n", range(1, constants.SIMULABLE_QUBITS - 3))
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
def test_exp_ZZFt_matrix(n: int, time: float):
    routine = routines.exp_ZZFt(n, time)
    ancilla_size = routine.arity - n - 2
    expiZZFt = expm(-1.0j * time * kron(Z, Z, F(n), *[I for _ in range(ancilla_size)]))
    implemented_matrix = routine2unitary(routines.exp_ZZFt(n, time))
    numpy.testing.assert_allclose(expiZZFt, implemented_matrix, atol=ABSOLUTE_TOLERANCE)


@needsqat
@pytest.mark.parametrize("r", range(1, (constants.SIMULABLE_QUBITS - 3) // 3))
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
def test_exp_ZZF2rt_matrix(r: int, time: float):
    routine = routines.exp_ZZF2r_t(r, time)
    ancilla_size = routine.arity - 3 * r - 2
    expiZZF2rt = expm(
        -1.0j
        * time
        * 2 ** (-2 * r)
        * kron(Z, Z, F(3 * r), *[I for _ in range(ancilla_size)])
    )
    implemented_matrix = routine2unitary(routine)
    numpy.testing.assert_allclose(
        expiZZF2rt, implemented_matrix, atol=bugfix_small_angles(ABSOLUTE_TOLERANCE)
    )
