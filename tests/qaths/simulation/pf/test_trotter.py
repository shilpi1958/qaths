# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================


import numpy
import pytest
import qaths.utils.constants
from qaths.applications.wave_equation.utils import (
    compute_qubit_number_from_considered_points_1d,
)
from qaths.bugfix.small_rotations_angle import bugfix_small_angles
from qaths.qram import OnesOrZerosSignQRAM
from qaths.simulation.base.integer_weighted import (
    simulate_signed_integer_weighted_hamiltonian,
)
from qaths.simulation.pf.trotter import simulate_using_trotter
from qaths.utils.math import expm
from qaths.utils.printing import transformation
from qaths.utils.quantum import assert_transformation_allclose
from qaths.utils.simulation import routine2unitary, chain_product

from tests.utils import constants
from tests.utils.decorators import needsqat, usessimulator
from tests.utils.matrices import construct_Dirichlet_Hamiltonians_1D


def _trotter_matrix(order: int, time: float, generators) -> numpy.ndarray:
    if not isinstance(generators, list):
        generators = list(generators)

    if order == 1:
        res = generators[0](time / 2)
        for generator in generators[1:-1]:
            res = numpy.dot(res, generator(time / 2))
        res = numpy.dot(res, generators[-1](time))
        for generator in reversed(generators[:-1]):
            res = numpy.dot(res, generator(time / 2))
        return res

    p_k = 1 / (4 - pow(4, 1 / (2 * order - 1)))
    rec1 = _trotter_matrix(order - 1, p_k * time, generators)
    rec2 = _trotter_matrix(order - 1, (1 - 4 * p_k) * time, generators)

    return chain_product([rec1, rec1, rec2, rec1, rec1])


@needsqat
@usessimulator
# @slow
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("discretisation_points_number", list(range(4, 6)))
@pytest.mark.parametrize("trotter_order", list(range(1, 4)))
def test_trotter_wave_solver_dirichlet(
    time: float, discretisation_points_number: int, trotter_order: int
):
    n = compute_qubit_number_from_considered_points_1d(discretisation_points_number - 2)
    # Computing the matrices and creating the matrix generators
    H1, H2 = [
        H.toarray()
        for H in construct_Dirichlet_Hamiltonians_1D(discretisation_points_number)
    ]

    eH1 = lambda t: expm(-1.0j * t * H1)
    eH2 = lambda t: expm(-1.0j * t * H2)

    # Computing the oracles. Here we use the QRAM because it does not need ancilla
    # qubits, which will speed-up the simulation.
    OH1, OH2 = OnesOrZerosSignQRAM(H1), OnesOrZerosSignQRAM(H2)
    # Circuit generators
    GH1 = lambda t: simulate_signed_integer_weighted_hamiltonian(OH1, n, 1, t)
    GH2 = lambda t: simulate_signed_integer_weighted_hamiltonian(OH2, n, 1, t)

    expected_matrix = _trotter_matrix(trotter_order, time, [eH1, eH2])
    simulation_routine = simulate_using_trotter(trotter_order, time, [GH1, GH2])

    simulated_full_matrix = routine2unitary(simulation_routine)

    # We restrict ourselves to the cases where all the qubits after |x> are in the
    # |0> state.
    rows_to_test = numpy.arange(
        0, 2 ** simulation_routine.arity, 2 ** (simulation_routine.arity - n)
    )

    simulated_matrix = simulated_full_matrix[rows_to_test, :][:, rows_to_test]

    print("Simulated: \n{}".format(transformation(simulated_matrix, decimals=8)))
    print("Expected:  \n{}".format(transformation(expected_matrix, decimals=8)))

    print("=" * 80)
    numpy.set_printoptions(precision=2, linewidth=300)
    print("Simulated:\n{}".format(simulated_matrix))
    print("Expected:\n{}".format(expected_matrix))
    print("Difference:\n{}".format(numpy.abs(simulated_matrix - expected_matrix)))
    assert_transformation_allclose(
        expected_matrix,
        simulated_matrix,
        atol=bugfix_small_angles(qaths.utils.constants.ABSOLUTE_TOLERANCE),
    )
