# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import itertools

import numpy
import pytest
import qaths.utils.constants
from qaths.applications.wave_equation.oracles import (
    get_oracle_dirichlet1_1d_wave_equation,
    get_oracle_dirichlet2_1d_wave_equation,
)
from qaths.applications.wave_equation.utils import (
    compute_qubit_number_from_considered_points_1d,
)
from qaths.simulation.base.integer_weighted import (
    simulate_signed_integer_weighted_hamiltonian,
)
from qaths.utils.math import reverse_normalised_kronecker, kron, expm
from qaths.utils.quantum import (
    qstate_msb,
    complete_by_zeros as complete_qstate_by_zeros,
    zero,
    assert_qstate_allclose,
)
from qaths.utils.simulation import simulate_routine

from tests.utils import constants
from tests.utils.decorators import needsqat, usessimulator, slow, randomised
from tests.utils.matrices import construct_Dirichlet_Hamiltonians_1D


@pytest.fixture(
    scope="module",
    params=itertools.product(list(range(4, 10)), constants.FLOAT_DISCRETISATION),
)
def first_hamiltonian_data_fixture(request):
    discretisation_points_number, time = request.param
    n = compute_qubit_number_from_considered_points_1d(discretisation_points_number - 2)

    oracle = get_oracle_dirichlet1_1d_wave_equation(n, discretisation_points_number)
    routine = simulate_signed_integer_weighted_hamiltonian(oracle, n, 1, time)

    H1 = construct_Dirichlet_Hamiltonians_1D(discretisation_points_number)[0].toarray()
    expected_evolution_on_input = expm(-1.0j * time * H1)
    print(H1)

    return n, discretisation_points_number, time, routine, expected_evolution_on_input


@pytest.fixture(
    scope="module",
    params=itertools.product(list(range(4, 10)), constants.FLOAT_DISCRETISATION),
)
def second_hamiltonian_data_fixture(request):
    discretisation_points_number, time = request.param
    n = compute_qubit_number_from_considered_points_1d(discretisation_points_number - 2)

    oracle = get_oracle_dirichlet2_1d_wave_equation(n, discretisation_points_number)
    routine = simulate_signed_integer_weighted_hamiltonian(oracle, n, 1, time)

    H2 = construct_Dirichlet_Hamiltonians_1D(discretisation_points_number)[1].toarray()
    expected_evolution_on_input = expm(-1.0j * time * H2)

    return n, discretisation_points_number, time, routine, expected_evolution_on_input


@needsqat
@usessimulator
@slow
def test_oracle_1D_dirichlet1_simulation(first_hamiltonian_data_fixture):
    (
        n,
        discretisation_points_number,
        time,
        routine,
        expected_evolution_on_input,
    ) = first_hamiltonian_data_fixture
    for x_index in range(2 ** n):
        # Create the states that will be used as inputs
        x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
        full_evolve_gate_input_state = complete_qstate_by_zeros(
            x_input_state, routine.arity - n
        )
        # Simulate the routines and recover the result
        full_simulated_output = simulate_routine(
            routine, starting_state=full_evolve_gate_input_state
        )
        simulated_output, ancilla_state = reverse_normalised_kronecker(
            full_simulated_output, 2 ** n, 2 ** (routine.arity - n)
        )
        expected_output = numpy.dot(expected_evolution_on_input, x_input_state)

        assert_qstate_allclose(
            ancilla_state, kron(*[zero for _ in range(routine.arity - n)])
        )
        assert_qstate_allclose(
            simulated_output,
            expected_output,
            atol=qaths.utils.constants.ABSOLUTE_TOLERANCE,
        )


@needsqat
@usessimulator
@slow
def test_oracle_1D_dirichlet2_simulation(second_hamiltonian_data_fixture):
    (
        n,
        discretisation_points_number,
        time,
        routine,
        expected_evolution_on_input,
    ) = second_hamiltonian_data_fixture
    for x_index in range(2 ** n):
        # Create the states that will be used as inputs
        x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
        full_evolve_gate_input_state = complete_qstate_by_zeros(
            x_input_state, routine.arity - n
        )
        # Simulate the routines and recover the result
        full_simulated_output = simulate_routine(
            routine, starting_state=full_evolve_gate_input_state
        )
        simulated_output, ancilla_state = reverse_normalised_kronecker(
            full_simulated_output, 2 ** n, 2 ** (routine.arity - n)
        )
        expected_output = numpy.dot(expected_evolution_on_input, x_input_state)

        assert_qstate_allclose(
            ancilla_state, kron(*[zero for _ in range(routine.arity - n)])
        )
        assert_qstate_allclose(
            simulated_output,
            expected_output,
            atol=qaths.utils.constants.ABSOLUTE_TOLERANCE,
        )


@needsqat
@usessimulator
@slow
@randomised(5)
def test_oracle_1D_dirichlet1_simulation_randomised(first_hamiltonian_data_fixture):

    (
        n,
        discretisation_points_number,
        time,
        routine,
        expected_evolution_on_input,
    ) = first_hamiltonian_data_fixture

    x_index = numpy.random.randint(2 ** n)
    # Create the states that will be used as inputs
    x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
    full_evolve_gate_input_state = complete_qstate_by_zeros(
        x_input_state, routine.arity - n
    )
    # Simulate the routines and recover the result
    full_simulated_output = simulate_routine(
        routine, starting_state=full_evolve_gate_input_state
    )
    simulated_output, ancilla_state = reverse_normalised_kronecker(
        full_simulated_output, 2 ** n, 2 ** (routine.arity - n)
    )
    expected_output = numpy.dot(expected_evolution_on_input, x_input_state)

    assert_qstate_allclose(
        ancilla_state, kron(*[zero for _ in range(routine.arity - n)])
    )
    assert_qstate_allclose(
        simulated_output, expected_output, atol=qaths.utils.constants.ABSOLUTE_TOLERANCE
    )


@needsqat
@usessimulator
@slow
@randomised(5)
def test_oracle_1D_dirichlet2_simulation_randomised(second_hamiltonian_data_fixture):

    (
        n,
        discretisation_points_number,
        time,
        routine,
        expected_evolution_on_input,
    ) = second_hamiltonian_data_fixture

    x_index = numpy.random.randint(2 ** n)
    # Create the states that will be used as inputs
    x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
    full_evolve_gate_input_state = complete_qstate_by_zeros(
        x_input_state, routine.arity - n
    )
    # Simulate the routines and recover the result
    full_simulated_output = simulate_routine(
        routine, starting_state=full_evolve_gate_input_state
    )
    simulated_output, ancilla_state = reverse_normalised_kronecker(
        full_simulated_output, 2 ** n, 2 ** (routine.arity - n)
    )
    expected_output = numpy.dot(expected_evolution_on_input, x_input_state)

    assert_qstate_allclose(
        ancilla_state, kron(*[zero for _ in range(routine.arity - n)])
    )
    assert_qstate_allclose(simulated_output, expected_output)
