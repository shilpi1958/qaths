# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy
import pytest
from qaths.applications.wave_equation.evolve_1D_dirichlet import evolve_1d_dirichlet
from qaths.applications.wave_equation.time_adaptation import (
    adapt_dirichlet_evolution_time,
)
from qaths.applications.wave_equation.utils import (
    compute_qubit_number_from_considered_points_1d,
)
from qaths.utils.math import reverse_normalised_kronecker, expm, kron
from qaths.utils.printing import msbqstate2str
from qaths.utils.quantum import (
    zero,
    qstate_msb,
    complete_by_zeros as complete_qstate_by_zeros,
    assert_qstate_allclose,
)
from qaths.utils.simulation import simulate_routine

from tests.utils import constants
from tests.utils.decorators import needsqat, usessimulator, slow, randomised
from tests.utils.matrices import construct_Dirichlet_Hamiltonians_1D


@needsqat
@usessimulator
@slow
@pytest.mark.parametrize("discretisation_points_number", [4, 6])
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("epsilon", constants.EPSILONS)
def test_evolve_1D_dirichlet(
    discretisation_points_number: int, time: float, epsilon: float
):
    n = compute_qubit_number_from_considered_points_1d(discretisation_points_number - 2)

    evolve_gate = evolve_1d_dirichlet(
        time, discretisation_points_number, epsilon, trotter_order=1
    )

    H1, H2 = construct_Dirichlet_Hamiltonians_1D(discretisation_points_number)
    H = H1.toarray() + H2.toarray()

    simulation_time = adapt_dirichlet_evolution_time(time, discretisation_points_number)

    expected_evolution_on_input = expm(-1.0j * simulation_time * H)

    for x_index in range(2 ** n):
        # Create the states that will be used as inputs
        x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
        full_evolve_gate_input_state = complete_qstate_by_zeros(
            x_input_state, evolve_gate.arity - n
        )
        # Simulate the routines and recover the result
        full_simulated_output = simulate_routine(
            evolve_gate, starting_state=full_evolve_gate_input_state
        )
        simulated_output, ancilla_state = reverse_normalised_kronecker(
            full_simulated_output, 2 ** n, 2 ** (evolve_gate.arity - n)
        )
        expected_output = numpy.dot(expected_evolution_on_input, x_input_state)

        assert_qstate_allclose(
            ancilla_state,
            kron(*[zero for _ in range(evolve_gate.arity - n)]),
            atol=epsilon,
        )

        print("Input:     {}".format(msbqstate2str(x_input_state)))
        print("Expected:  {}".format(msbqstate2str(expected_output)))
        print("Simulated: {}".format(msbqstate2str(simulated_output)))

        assert_qstate_allclose(simulated_output, expected_output, atol=epsilon)


@needsqat
@usessimulator
@pytest.mark.parametrize("discretisation_points_number", [4, 6])
@pytest.mark.parametrize("time", constants.FLOAT_DISCRETISATION)
@pytest.mark.parametrize("epsilon", constants.EPSILONS)
@randomised(3)
def test_evolve_1D_dirichlet_randomised(
    discretisation_points_number: int, time: float, epsilon: float
):
    n = compute_qubit_number_from_considered_points_1d(discretisation_points_number - 2)

    evolve_gate = evolve_1d_dirichlet(
        time, discretisation_points_number, epsilon, trotter_order=1
    )

    H1, H2 = construct_Dirichlet_Hamiltonians_1D(discretisation_points_number)
    H = H1.toarray() + H2.toarray()

    # We should simulate 1/a * H for a time t. The 1/a factor can be pushed in
    # the simulation time, which left us with the problem of simulating H for a
    # time t/a. See the remark in arXiv:1711.05394v1 (2nd paragraph, page 7) for
    # details about the value of a.
    a = 1 / (
        discretisation_points_number
        + -1  # Minus one to get the number of edges in the graph.
    )
    simulation_time = time / a

    expected_evolution_on_input = expm(-1.0j * simulation_time * H)

    x_index = numpy.random.randint(2 ** n)

    # Create the states that will be used as inputs
    x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
    # Simulate the routines and recover the result
    full_simulated_output = simulate_routine(evolve_gate, starting_state=x_input_state)

    simulated_output, ancilla_state = reverse_normalised_kronecker(
        full_simulated_output, 2 ** n, 2 ** (evolve_gate.arity - n)
    )
    expected_output = numpy.dot(expected_evolution_on_input, x_input_state)

    assert_qstate_allclose(
        ancilla_state, kron(*[zero for _ in range(evolve_gate.arity - n)]), atol=epsilon
    )
    print("Expected:  {}".format(msbqstate2str(expected_output)))
    print("Simulated: {}".format(msbqstate2str(simulated_output)))

    assert_qstate_allclose(simulated_output, expected_output, atol=epsilon)
