# ======================================================================
# Copyright CERFACS (May 2019)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This software is governed by the CeCILL-B license under French law and
# abiding  by the  rules of  distribution of free software. You can use,
# modify  and/or  redistribute  the  software  under  the  terms  of the
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
# URL "http://www.cecill.info".
#
# As a counterpart to the access to  the source code and rights to copy,
# modify and  redistribute granted  by the  license, users  are provided
# only with a limited warranty and  the software's author, the holder of
# the economic rights,  and the  successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using, modifying and/or  developing or reproducing  the
# software by the user in light of its specific status of free software,
# that  may mean  that it  is complicated  to manipulate,  and that also
# therefore  means that  it is reserved for  developers and  experienced
# professionals having in-depth  computer knowledge. Users are therefore
# encouraged  to load and  test  the software's  suitability as  regards
# their  requirements  in  conditions  enabling  the  security  of their
# systems  and/or  data to be  ensured and,  more generally,  to use and
# operate it in the same conditions as regards security.
#
# The fact that you  are presently reading this  means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.
# ======================================================================

import numpy
import pytest
from qaths.applications.wave_equation.oracles import (
    get_oracle_dirichlet1_1d_wave_equation,
    get_oracle_dirichlet2_1d_wave_equation,
)
from qaths.applications.wave_equation.utils import (
    compute_qubit_number_from_considered_points_1d,
)
from qaths.qram import OnesOrZerosSignQRAM
from qaths.utils import constants
from qaths.utils.math import reverse_normalised_kronecker, kron
from qaths.utils.quantum import (
    qstate_msb,
    complete_by_zeros as complete_qstate_by_zeros,
    zero,
)
from qaths.utils.simulation import simulate_routine

from tests.utils.decorators import needsqat, usessimulator, slow
from tests.utils.matrices import construct_Dirichlet_Hamiltonians_1D


@needsqat
@usessimulator
@slow
@pytest.mark.parametrize("discretisation_points_number", [4, 6])
def test_oracle_dirichlet_1(discretisation_points_number: int):
    n = compute_qubit_number_from_considered_points_1d(discretisation_points_number - 2)
    routine = get_oracle_dirichlet1_1d_wave_equation(n, discretisation_points_number)

    H = construct_Dirichlet_Hamiltonians_1D(discretisation_points_number)[0].toarray()

    QRAM = OnesOrZerosSignQRAM(H)

    oracle_ancilla_number = routine.arity - QRAM.arity

    for x_index in range(2 ** n):
        # Create the states that will be used as inputs
        x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
        full_qram_input_state = complete_qstate_by_zeros(x_input_state, QRAM.arity - n)
        full_routine_input_state = complete_qstate_by_zeros(
            x_input_state, routine.arity - n
        )
        # Simulate the routines and recover the result
        expected_output = simulate_routine(QRAM, starting_state=full_qram_input_state)
        simulated_output = simulate_routine(
            routine, starting_state=full_routine_input_state
        )

        # If there is at least one non-zero entry
        if numpy.any(numpy.abs(H[x_index]) > constants.UNDER_IS_ZERO):
            # Then the 2 outputs should be **exactly** the same up to ancillas
            interesting_state, ancillas_state = reverse_normalised_kronecker(
                simulated_output, 2 ** QRAM.arity, 2 ** oracle_ancilla_number
            )
            numpy.testing.assert_allclose(
                ancillas_state,
                kron(*[zero for _ in range(oracle_ancilla_number)]),
                atol=constants.ABSOLUTE_TOLERANCE,
            )
            numpy.testing.assert_allclose(
                interesting_state, expected_output, atol=constants.ABSOLUTE_TOLERANCE
            )
        # Else if there is no non-zero entry, the only constraints are:
        # 1. The input register |x> is preserved
        # 2. The weight register |w> should be equal to 0.
        # 3. The ancilla register |a> should be equal to 0.
        # The 2 other registers (|m> and |s>) can be anything. In the end we have
        # |x>|m=0>|v=0>|s=0>|a=0>  -->  |x>|m=?>|v=0>|s=?>|a=0>
        else:
            xsim, _, vsim, _, asim = reverse_normalised_kronecker(
                simulated_output,
                2 ** n,
                2 ** n,
                2 ** 1,
                2 ** 1,
                2 ** oracle_ancilla_number,
            )
            numpy.testing.assert_allclose(
                xsim, x_input_state, atol=constants.ABSOLUTE_TOLERANCE
            )
            numpy.testing.assert_allclose(vsim, zero, atol=constants.ABSOLUTE_TOLERANCE)
            numpy.testing.assert_allclose(
                asim,
                kron(*[zero for _ in range(oracle_ancilla_number)]),
                atol=constants.ABSOLUTE_TOLERANCE,
            )


@needsqat
@usessimulator
@slow
@pytest.mark.parametrize("discretisation_points_number", [4, 6])
def test_oracle_dirichlet_2(discretisation_points_number: int):
    n = compute_qubit_number_from_considered_points_1d(discretisation_points_number - 2)
    routine = get_oracle_dirichlet2_1d_wave_equation(n, discretisation_points_number)

    H = construct_Dirichlet_Hamiltonians_1D(discretisation_points_number)[1].toarray()
    QRAM = OnesOrZerosSignQRAM(H)

    oracle_ancilla_number = routine.arity - QRAM.arity

    for x_index in range(2 ** n):
        # Create the states that will be used as inputs
        x_input_state = qstate_msb(bin(x_index)[2:].zfill(n))
        full_qram_input_state = complete_qstate_by_zeros(x_input_state, QRAM.arity - n)
        full_routine_input_state = complete_qstate_by_zeros(
            x_input_state, routine.arity - n
        )
        # Simulate the routines and recover the result
        expected_output = simulate_routine(QRAM, starting_state=full_qram_input_state)
        simulated_output = simulate_routine(
            routine, starting_state=full_routine_input_state
        )

        # If there is at least one non-zero entry
        if numpy.any(numpy.abs(H[x_index]) > constants.UNDER_IS_ZERO):
            # Then the 2 outputs should be **exactly** the same up to ancillas
            interesting_state, ancillas_state = reverse_normalised_kronecker(
                simulated_output, 2 ** QRAM.arity, 2 ** oracle_ancilla_number
            )
            numpy.testing.assert_allclose(
                ancillas_state,
                kron(*[zero for _ in range(oracle_ancilla_number)]),
                atol=constants.ABSOLUTE_TOLERANCE,
            )
            numpy.testing.assert_allclose(
                interesting_state, expected_output, atol=constants.ABSOLUTE_TOLERANCE
            )
        # Else if there is no non-zero entry, the only constraints are:
        # 1. The input register |x> is preserved
        # 2. The weight register |w> should be equal to 0.
        # 3. The ancilla register |a> should be equal to 0.
        # The 2 other registers (|m> and |s>) can be anything. In the end we have
        # |x>|m=0>|v=0>|s=0>|a=0>  -->  |x>|m=?>|v=0>|s=?>|a=0>
        else:
            xsim, _, vsim, _, asim = reverse_normalised_kronecker(
                simulated_output,
                2 ** n,
                2 ** n,
                2 ** 1,
                2 ** 1,
                2 ** oracle_ancilla_number,
            )
            numpy.testing.assert_allclose(
                xsim, x_input_state, atol=constants.ABSOLUTE_TOLERANCE
            )
            numpy.testing.assert_allclose(vsim, zero, atol=constants.ABSOLUTE_TOLERANCE)
            numpy.testing.assert_allclose(
                asim,
                kron(*[zero for _ in range(oracle_ancilla_number)]),
                atol=constants.ABSOLUTE_TOLERANCE,
            )
