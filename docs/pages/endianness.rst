..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

Note on endianness
##################

Endianness is **not** abstracted by the Quantum Learning Machine and may become
a recurrent problem if no convention is fixed on the entire project.

This page contain the convention used by the `qaths` library.

Quantum Learning Machine internal endianness
--------------------------------------------

This section precise the endianness used internally by the Quantum Learning
Machine.

Known bugs
~~~~~~~~~~

#. The linear algebra simulator is known to return a swapped result when an oracle
   has been used in the simulated circuit. The bug has been verified with the
   `StatePreparation` gate and may also affect all the other oracles in
   `qat.lang.AQASM.oracle`. A fix will be soon available.

Arithmetic functions
~~~~~~~~~~~~~~~~~~~~

All the arithmetic functions that interpret quantum registers as integers are
designed to work in **little-endian**.


`qaths` endianness conventions
------------------------------

Tests
~~~~~

`qaths`' tests should use the **big-endian** convention **everywhere**.

.. admonition:: Why not little-endian?

   Most of the time, byte-order is just a convention and there is not a specific
   reason to prefer one byte-order to the other.

   Within tests, the big-endian order is the most convenient because:

   #. Humans (at least the ones using Arabic numerals) are used to read numbers
      in big-endian.
   #. The tensor operations to construct matrices or quantum states will follow
      the big-endian convention and so will be easier to read (see 1.).

Library
~~~~~~~

`qaths` should use the **big-endian** convention **everywhere** except:

#. When one of the used external library use little-endian. In this particular case,
   the method calling the external library should expose a big-endian API (i.e. input
   is given in big-endian, output is returned as big-endian) but may use a little-endian
   representation internally.

   .. warning::

      The fact that the method is calling a little-endian function should be entirely
      hidden to the user. The method should handle **transparently** the endianness
      of the internal functions.
