..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

❌Linear combinations of quantum walk steps
===========================================

Main article: :cite:`1501.01715v3`.

Applicability domain
--------------------

All the :math:`s`-sparse Hamiltonian matrices :math:`H` are simulable with the
quantum-walk algorithm, even if the matrix is not sparse (i.e.
even if :math:`s \not\in \mathcal{O}(\text{poly}(\log N))`), provided that the
following unitary operators are given:

* :math:`O_H`, the black-box acting as

  .. math::

     O_H \vert j,k\rangle \vert z \rangle =
     \vert j,k \rangle \vert z \oplus H_{j,k} \rangle.

* :math:`O_F`, the black-box acting as

  .. math::

     O_F \vert j,k \rangle = \vert j, f(j, k) \rangle,

  where :math:`f(j,k)` is the row (resp. column) index of the :math:`k^\text{th}`
  nonzero element of the column (resp. row) :math:`j`.


Complexity
----------

A :math:`s`-sparse Hamiltonian :math:`H` acting on :math:`n` qubits can be
simulated for time :math:`t` within error :math:`\epsilon` with

.. math::

   \mathcal{O}\left( \tau \frac{\log \frac{\tau}{\epsilon}}{\log \log \frac{\tau}{\epsilon}} \right)

queries and

.. math::

   \mathcal{O}\left( \tau \left[ n + \log^{5/2}\left( \frac{\tau}{\epsilon} \right) \right]
   \frac{\log \frac{\tau}{\epsilon}}{\log \log \frac{\tau}{\epsilon}} \right)

additional 2-qubit gates, where :math:`\tau = s \vert\vert H \vert\vert_{max}t`.

A slight modification to the original algorithm can achieve a query complexity of

.. math::

   \mathcal{O} \left( \tau^{1+\alpha/2} + \tau^{1-\alpha/2} \log \left(1/\epsilon\right) \right)

with :math:`\alpha \in (0, 1]`.

Available implementations
-------------------------

*No known implementation.*
