..
    Copyright CERFACS (December 2018)
    Contributor: Adrien Suau (adrien.suau@cerfacs.fr)

    This software is governed by the CeCILL-B license under French law and
    abiding  by the  rules of  distribution of free software. You can use,
    modify  and/or  redistribute  the  software  under  the  terms  of the
    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    URL "http://www.cecill.info".

    As a counterpart to the access to  the source code and rights to copy,
    modify and  redistribute granted  by the  license, users  are provided
    only with a limited warranty and  the software's author, the holder of
    the economic rights,  and the  successive licensors  have only limited
    liability.

    In this respect, the user's attention is drawn to the risks associated
    with loading,  using, modifying and/or  developing or reproducing  the
    software by the user in light of its specific status of free software,
    that  may mean  that it  is complicated  to manipulate,  and that also
    therefore  means that  it is reserved for  developers and  experienced
    professionals having in-depth  computer knowledge. Users are therefore
    encouraged  to load and  test  the software's  suitability as  regards
    their  requirements  in  conditions  enabling  the  security  of their
    systems  and/or  data to be  ensured and,  more generally,  to use and
    operate it in the same conditions as regards security.

    The fact that you  are presently reading this  means that you have had
    knowledge of the CeCILL-B license and that you accept its terms.

❌Algorithms based on Uniform Spectral Amplification
====================================================

Main article: :cite:`1707.05391v1`.

Applicability domain
--------------------

All the :math:`s`-sparse Hamiltonian matrices :math:`H` are simulable with the
uniform spectral amplification algorithm provided that the following unitary
operators (oracles) are given:

* :math:`O_H`, the black-box acting as

  .. math::

     O_H \vert j,k\rangle \vert z \rangle =
     \vert j,k \rangle \vert z \oplus H_{j,k} \rangle.

* :math:`O_F`, the black-box acting as

  .. math::

     O_F \vert j,k \rangle = \vert j, f(j, k) \rangle,

  where :math:`f(j,k)` is the row (resp. column) index of the :math:`k^\text{th}`
  nonzero element of the column (resp. row) :math:`j`.


Complexity
----------

From :cite:`1707.05391v1` (Theorem 5), the uniform spectral amplification algorithm
requires

.. math::

   \mathcal{O} \left( t \left( s \vert\vert\hat{H}\vert\vert_{max} \vert\vert\hat{H}\vert\vert_1 \right)^{1/2}
   \log \left( \frac{t\vert\vert\hat{H}\vert\vert}{\epsilon} \right) \left( 1 +
   \frac{1}{t\vert\vert\hat{H}\vert\vert_1} \frac{\log \frac{1}{\epsilon}}{\log \log \frac{1}{\epsilon}}\right)\right)

queries to the oracles :math:`O_H` and :math:`O_F` with:

* :math:`t` the simulation time
* :math:`\hat{H}` the simulated Hamiltonian
* :math:`s` the sparsity of :math:`\hat{H}`
* :math:`\vert\vert\hat{H}\vert\vert_{max} = \max_{j,k} \vert \hat{H}_{j,k}\vert`
* :math:`\vert\vert\hat{H}\vert\vert_{1} = \max_{j} \sum_k \vert \hat{H}_{j,k}\vert`
* :math:`\vert\vert\hat{H}\vert\vert` the spectral-norm of :math:`\hat{H}`
* :math:`\epsilon` the allowed error


Available implementations
-------------------------

*No known implementation.*
